// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getFirestore } from "firebase/firestore";

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyBrozE2eYPTE0HXzkXHYBjK2EE8EWI6K1o",
  authDomain: "messenger-379fd.firebaseapp.com",
  projectId: "messenger-379fd",
  storageBucket: "messenger-379fd.appspot.com",
  messagingSenderId: "618351850608",
  appId: "1:618351850608:web:4e22918176c21f39b0b0b5",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const auth = getAuth(app);
const db = getFirestore(app);
export { app, auth, db };
