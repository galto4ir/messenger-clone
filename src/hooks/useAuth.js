// Hook (use-auth.js)
import React, { useState, useEffect, useContext, createContext } from "react";

import { auth } from "../lib/firebase";
import {
  createUserWithEmailAndPassword,
  onAuthStateChanged,
  signInWithEmailAndPassword,
  signOut,
} from "firebase/auth";
import usersApi from "../api/users";

const authContext = createContext();
// Provider component that wraps your app and makes auth object ...
// ... available to any child component that calls useAuth().
export function ProvideAuth({ children }) {
  const authData = useProvideAuth();
  return (
    <authContext.Provider value={authData}>{children}</authContext.Provider>
  );
}
// Hook for child components to get the auth object ...
// ... and re-render when it changes.
export const useAuth = () => {
  return useContext(authContext);
};
// Provider hook that creates auth object and handles state
function useProvideAuth() {
  const [user, setUser] = useState(null);
  // Wrap any Firebase methods we want to use making sure ...
  // ... to save the user to state.
  const signin = (email, password) => {
    return signInWithEmailAndPassword(auth, email, password).then(
      (response) => {
        // setUser(response.user);
        return response.user;
      }
    );
  };
  const signup = (email, password, firstName, lastName) => {
    return createUserWithEmailAndPassword(auth, email, password).then(
      (response) => {
        const { user } = response;
        usersApi.init(user.uid, email, firstName, lastName).then((res) => {
          if (res.success) {
            setUser({ firstName, lastName, email, uid: user.uid });
          } else {
            console.log("ERROR: ", res);
          }
        });
        return response.user;
      }
    );
  };
  const signout = () => {
    return signOut(auth).then(() => {
      setUser(false);
    });
  };
  // const sendPasswordResetEmail = (email) => {
  //   return firebase
  //     .auth()
  //     .sendPasswordResetEmail(email)
  //     .then(() => {
  //       return true;
  //     });
  // };
  // const confirmPasswordReset = (code, password) => {
  //   return firebase
  //     .auth()
  //     .confirmPasswordReset(code, password)
  //     .then(() => {
  //       return true;
  //     });
  // };
  // Subscribe to user on mount
  // Because this sets state in the callback it will cause any ...
  // ... component that utilizes this hook to re-render with the ...
  // ... latest auth object.
  useEffect(() => {
    const unsubscribe = onAuthStateChanged(auth, (user) => {
      if (user) {
        usersApi.findOne(user.uid).then((res) => {
          if (res.success) {
            setUser({ ...res.data, uid: user.uid });
          } else {
            console.log("ERROR: ", res);
          }
        });
      } else {
        setUser(false);
      }
    });
    // Cleanup subscription on unmount
    return () => unsubscribe();
  }, []);

  useEffect(() => {
    console.log(user);
  }, [user]);
  // Return the user object and auth methods
  return {
    user,
    signin,
    signup,
    signout,
    // sendPasswordResetEmail,
    // confirmPasswordReset,
  };
}
