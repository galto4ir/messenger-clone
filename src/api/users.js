import { auth, db } from "../lib/firebase";
import {
  doc,
  setDoc,
  getDoc,
  getDocs,
  collection,
  query,
  where,
  onSnapshot,
  addDoc,
} from "firebase/firestore";
const usersApi = {
  init: async (uid, email, firsName, lastName) => {
    try {
      await setDoc(doc(db, "users", uid), {
        email,
        firsName,
        lastName,
      });
      return { success: true };
    } catch (e) {
      return { success: false, e };
    }
  },
  findOne: async (uid) => {
    try {
      const userRef = doc(db, "users", uid);
      const user = await getDoc(userRef);
      return { success: true, data: user.data() };
    } catch (e) {
      return { success: false, e };
    }
  },
  find: async () => {
    try {
      const currentUser = auth.currentUser;
      const usersRef = collection(db, "users");
      const users = await getDocs(usersRef);
      return {
        success: true,
        data: users.docs
          .map((doc) => ({ uid: doc.id, ...doc.data() }))
          .filter((user) => user && user.uid !== currentUser.uid),
      };
    } catch (e) {
      console.log("user: ", e);
      return { success: false, e };
    }
  },
  sendFriendRequest: async (from, to) => {
    try {
      await setDoc(doc(db, "friends", `${from}-${to}`), {
        from,
        to,
        accepted: false,
      });
      return { success: true };
    } catch (e) {
      return { success: false };
    }
  },
  accept: async (from, to) => {
    try {
      await setDoc(
        doc(db, "friends", `${from}-${to}`),
        {
          accepted: true,
        },
        { merge: true }
      );
      return { success: true };
    } catch (e) {
      return { success: false };
    }
  },
  checkFriendRequest: async (from, to) => {
    try {
      const friendRef = doc(db, "friends", `${from}-${to}`);
      const friend = await getDoc(friendRef);
      if (friend.exists) {
        const data = friend.data();
        return { success: true, data };
      } else {
        return { success: false };
      }
    } catch (e) {
      return { success: false };
    }
  },
  getNotifications: async () => {
    try {
      const currentUser = auth.currentUser;
      console.log("currentUser: ", currentUser);
      const notifRef = collection(db, "friends");
      const notifQuery = query(
        notifRef,
        where("to", "==", currentUser.uid),
        where("accepted", "==", false)
      );
      const notif = await getDocs(notifQuery);
      return {
        success: true,
        data: notif.docs.map((d) => ({ id: d.id, ...d.data() })),
      };
    } catch (e) {
      return { success: false, e };
    }
  },

  listenUsers: (cb) => {
    const user = auth.currentUser;
    const q = query(
      collection(db, "friends"),
      where("accepted", "==", true),
      where("to", "==", user.uid)
    );
    return onSnapshot(q, (querySnapshot) => {
      const users = [];
      querySnapshot.forEach((doc) => {
        users.push(doc.data());
      });
      cb(users);
    });
  },

  sendMessage: async (from, to, message) => {
    try {
      await addDoc(collection(db, "messages"), {
        from,
        to,
        message,
      });
    } catch (e) {}
  },
};

export default usersApi;
