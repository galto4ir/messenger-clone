import * as React from "react";
import usersApi from "../api/users";
import { useAuth } from "../hooks/useAuth";
const NewFriend = (props) => {
  const auth = useAuth();
  const [user, setUser] = React.useState(false);
  const [loading, setLoading] = React.useState(false);
  const acceptRequest = async () => {
    await usersApi.accept(props.uid, auth.user.uid);
    alert("Амжилттай");
  };

  React.useEffect(() => {
    const fetch = async () => {
      const res = await usersApi.findOne(props.uid);
      if (res.success) {
        setUser(res.data);
      }
    };
    fetch();
  }, []);

  return (
    <div className="border flex items-center justify-between p-10">
      <div>
        <h3>{`${user.firsName} ${user.lastName}`}</h3>
        <p>{user.email}</p>
      </div>
      <button
        onClick={acceptRequest}
        className="bg-blue-500 text-white rounded px-5 py-2 hover:bg-blue-400"
      >
        Зөвшөөрөх
      </button>
    </div>
  );
};

export default NewFriend;
