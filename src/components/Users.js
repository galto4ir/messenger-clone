import * as React from "react";
import usersApi from "../api/users";
import User from "./User";

const Users = ({ query }) => {
  const [users, setUsers] = React.useState([]);
  React.useEffect(() => {
    const fetch = async () => {
      const res = await usersApi.find();
      if (res.success) {
        setUsers(res.data);
      } else {
        console.log("ERROR: ", res);
      }
    };
    fetch();
  }, []);

  return (
    <section>
      {users
        .filter((user) => {
          if (user.firsName.indexOf(query) !== -1) return true;
          if (user.lastName.indexOf(query) !== -1) return true;
          if (user.email.indexOf(query) !== -1) return true;
          return false;
        })
        .map((user) => (
          <User {...user} />
        ))}
    </section>
  );
};

export default Users;
