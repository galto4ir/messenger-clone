import * as React from "react";
import { useAuth } from "../hooks/useAuth";
import Users from "./Users";
const Header = (props) => {
  const [showResult, setShowResult] = React.useState(false);
  const { user, signout } = useAuth();
  const [query, setQuery] = React.useState("");
  return (
    <section className="flex justify-between p-5 border-b">
      <img src="/logo.png" className="h-10" />
      <div className="flex relative items-center space-x-4 w-1/3">
        <span class="mi material-icons-outlined text-gray-500">search</span>
        <input
          placeholder="Search ..."
          className="focus:outline-none w-full"
          onChange={(e) => {
            setQuery(e.target.value);
          }}
          onBlur={() => {
            setShowResult(false);
          }}
          onFocus={() => {
            setShowResult(true);
          }}
        />
        <div
          className={`absolute transition-all top-16 left-0 right-0 ${
            showResult ? "opacity-100" : "opacity-0 invisible"
          }`}
        >
          <Users query={query} />
        </div>
      </div>
      <div>
        <div
          onClick={signout}
          className="h-10 w-10 bg-blue-500 flex items-center justify-center rounded-full text-white uppercase"
        >
          {user && user.email.substr(0, 2)}
        </div>
      </div>
    </section>
  );
};

export default Header;
