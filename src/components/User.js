import * as React from "react";
import usersApi from "../api/users";
import { useAuth } from "../hooks/useAuth";
const User = (user) => {
  const [sent, setSent] = React.useState(false);
  const { user: authUser } = useAuth();
  const [loading, setLoading] = React.useState(false);

  const sendFriendRequest = async () => {
    setLoading(true);
    const res = await usersApi.sendFriendRequest(authUser.uid, user.uid);
    if (res.success) {
      alert("Амжилттай хүсэлт явлаа!");
    }
    setLoading(false);
  };

  React.useEffect(() => {
    const fetch = async () => {
      const res = await usersApi.checkFriendRequest(authUser.uid, user.uid);
      if (res.success) {
        setSent(res.data);
      }
    };
    fetch();
  }, []);

  return (
    <div className="border flex items-center justify-between p-10">
      <div>
        <h3>{`${user.firsName} ${user.lastName}`}</h3>
        <p>{user.email}</p>
      </div>
      {sent ? (
        <div>
          {sent.accepted ? (
            <span>Таны найз</span>
          ) : (
            <span>Найзын хүсэлт явуулсан</span>
          )}
        </div>
      ) : (
        <div>
          {loading ? (
            <span>Найзын хүсэлт явуулж байна!</span>
          ) : (
            <button
              onClick={sendFriendRequest}
              className="bg-blue-500 text-white rounded px-5 py-2 hover:bg-blue-400"
            >
              Найз болох
            </button>
          )}
        </div>
      )}
    </div>
  );
};

export default User;
