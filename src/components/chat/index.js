import * as React from "react";
import ChatUser from "./ChatUser";

const Chat = (props) => {
  return (
    <section className="flex items-stretch bg-blue-300 h-screen border">
      <div className="w-1/4 border-r h-full">
        {props.friends.map((f) => (
          <ChatUser uid={f.from} />
        ))}
      </div>
      <div className="w-3/4 h-full">Chat Screen</div>
    </section>
  );
};

export default Chat;
