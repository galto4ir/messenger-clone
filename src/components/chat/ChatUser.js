import * as React from "react";
import usersApi from "../../api/users";

const ChatUser = (props) => {
  const [user, setUser] = React.useState(false);
  const [loading, setLoading] = React.useState(false);

  React.useEffect(() => {
    const fetch = async () => {
      const res = await usersApi.findOne(props.uid);
      if (res.success) {
        setUser(res.data);
      }
    };
    fetch();
  }, []);
  return (
    <div className="border-b p-5 bg-white cursor-pointer">
      <h3 className="font-bold">{`${user.firsName} ${user.lastName}`}</h3>
      <p>{user.email}</p>
    </div>
  );
};

export default ChatUser;
