import * as React from "react";
import { useNavigate } from "react-router";
import usersApi from "../api/users";
import Header from "../components/Header";
import Users from "../components/Users";
import NewFriend from "../components/NewFriend";
import { useAuth } from "../hooks/useAuth";
import Chat from "../components/chat";

const Home = () => {
  const [notif, setNotif] = React.useState([]);
  const [friends, setFriends] = React.useState([]);
  const navigate = useNavigate();
  const { user, signout } = useAuth();
  React.useEffect(() => {
    if (!user) {
      navigate("/signin", { replace: true });
    } else {
      const fetch = async () => {
        const res = await usersApi.getNotifications();
        if (res.success) {
          setNotif(res.data);
        } else {
          console.log("GET NOTIF: ", res);
        }
      };
      fetch();

      usersApi.listenUsers((users) => {
        setFriends(users);
      });
    }
  }, [user]);

  return (
    <main>
      <Header />
      <fieldset className="p-5 border">
        <legend>Мэдэгдэл</legend>
        {notif.map((n) => (
          <NewFriend uid={n.from} />
        ))}
      </fieldset>
      <Chat friends={friends} />
    </main>
  );
};

export default Home;
