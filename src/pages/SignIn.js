import React, { useEffect, useState } from "react";
import { useFormik } from "formik";
import { useAuth } from "../hooks/useAuth";
import { useNavigate } from "react-router";

const SignIn = () => {
  const navigate = useNavigate();
  const { user, signin } = useAuth();
  // Note that we have to initialize ALL of fields with values. These
  // could come from props, but since we don’t want to prefill this form,
  // we just use an empty string. If we don’t do this, React will yell
  // at us.
  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    onSubmit: (values) => {
      return signin(values.email, values.password).then(() => {
        navigate("/", { replace: true });
      });
    },
  });

  useEffect(() => {
    if (user) {
      navigate("/", { replace: true });
    }
  }, [user]);

  return (
    <div className="container mx-auto">
      <form
        onSubmit={formik.handleSubmit}
        className="flex flex-col w-96 mx-auto"
      >
        <label htmlFor="email">Email Address</label>
        <input
          id="email"
          name="email"
          type="email"
          onChange={formik.handleChange}
          value={formik.values.email}
          className="border p-2"
        />

        <label htmlFor="password">Password</label>
        <input
          id="password"
          name="password"
          type="password"
          onChange={formik.handleChange}
          value={formik.values.password}
          className="border p-2"
        />

        <button type="submit">
          {formik.isSubmitting ? "Loading" : "Submit"}
        </button>
      </form>
    </div>
  );
};

export default SignIn;
